const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config')


const mongoose = require('mongoose')
const UserModel = mongoose.model("UserModel")


module.exports = (req, res, next) =>{

    const { authorization} = req.headers;
    if(!authorization){

        return res.status(401).json({ error: 'Login required'})

    }

    const tokenValue = authorization.replace("Bearer ", "")
    jwt.verify(tokenValue, JWT_SECRET, (error, payload) =>{
        if(error){
            return res.status(401).json({ error: 'Login required'})

        }


        const {_id} = payload

        UserModel.find(_id).then(user => {
            req.userData = user
        })
    })

    next()

}