const express = require('express')
const bcrypt = require('bcryptjs')
const router = express.Router()
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config')
const protectedRoute = require('../middleware/protectedRoute')

const mongoose = require('mongoose')
const UserModel = mongoose.model("UserModel")

router.get('/home', protectedRoute, (req, res) => {
    res.send("Welcome To Your Home!")
})

router.post('/register', (req, res) => {
    //console.log(req.body.fullName)
    const { fullName, email, password } = req.body
    if (!fullName || !email || !password) {
        return res.status(400).json({ error: "Mandatory fields cannot be empty" })
    }
    UserModel.findOne({ email: email })
        .then((dbUser) => {
            if (dbUser) {
                return res.status(400).json({ error: "User already exist" })
            }
            bcrypt.hash(password, 16)
                .then(hp => {
                    const userData = new UserModel({
                        fullName,
                        email,
                        password: hp

                    })
                    userData.save()
                        .then(result => {
                            res.status(201).json({ result: "User registered successfully" })
                        })
                        .catch(error => {
                            console.log(error)
                        })
                })

        }).catch(error => {
            console.log(error)
        })

})

router.post('/login', (req, res) => {
    //console.log(req.body.fullName)
    const { email, password } = req.body
    if (!email || !password) {
        return res.status(400).json({ error: "Mandatory fields cannot be empty" })
    }
    UserModel.findOne({ email: email })
        .then((dbUser) => {
            
            bcrypt.compare(password, dbUser.password)
                .then(didMatch => {

                    if(didMatch){
                        const jwtToken = jwt.sign({ _id:dbUser._id}, JWT_SECRET)
                        res.json({ token: jwtToken})
                    }else{
                        return res.status(401).json({ error: "Invalid Credentials" })
                    }
                    
                }).catch(error => {
                    console.log(error)
                })

        }).catch(error => {
            console.log(error)
        })

})

module.exports = router